
/** Top Nav
 **************************************************************** **/
vnTRUST.topNav = function () {

};

/** init Load
 **************************************************************** **/
vnTRUST.init = function () {
  var Xwidth = $(window).width();
  if (Xwidth < 1100) {
    $(".floating-left").hide();
    $(".floating-right").hide()
  }
  vnTRUST.topNav();

  $(".fancybox").fancybox();

  $(".alert-autohide").delay(5000).slideUp(200, function () {
    $(this).alert('close');
  });


  $(window).resize(function(){

  });

  $(".menu-category .mc-title").click(function (e) {
    if(! $(this).parents(".menu-category").hasClass("active")){
      $(this).parents(".menu-category").addClass("active");
    }else{
      $(this).parents(".menu-category").removeClass("active");
    }
  });
  $(window).bind("click",function (e) {
    var $clicked = $(e.target);
    if(! $clicked.parents().hasClass("menu-category")){
      $(".menu-category").removeClass("active");
    }
  });


  //vnTRUST.load_Statistics();
  //vnTRUST.show_popupBanner(10000);
  //vnTRUST.ZoomImage();
  //vnTRUST.goTopStart();

};

/* Init */
jQuery(window).ready(function () {
  vnTRUST.init();
});